package gui;

import org.openqa.selenium.Keys;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;

import java.util.ArrayList;
import java.util.Map;

import logic.Expectimax;
import logic.State;

public class GameController{

    private ChromeDriver driver;

    public GameController() {
        String chromeBinary = "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe";
        System.setProperty("webdriver.chrome.driver", "D:\\Development\\it3105\\chromedriver.exe");
        ChromeOptions options = new ChromeOptions();
        options.setBinary(chromeBinary);
        driver = new ChromeDriver(options);
    }

    public void setup() {
        driver.get("http://gabrielecirulli.github.io/2048/");
        driver.executeScript("window.tmp = GameManager.prototype.isGameTerminated;");
        driver.executeScript("GameManager.prototype.isGameTerminated = function() {GameManager._instance = this; return true;}");
        Actions actions = new Actions(driver);
        actions.sendKeys(Keys.ARROW_LEFT).perform();
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {}
        driver.executeScript("GameManager.prototype.isGameTerminated = window.tmp");
        actions.sendKeys(Keys.ARROW_UP).perform();
    }

    public void stop() {
        driver.close();
    }

    public void move(int move) {
        driver.executeScript(String.format("GameManager._instance.move(%d)", move));
    }
        
    @SuppressWarnings("unchecked")
	public long getBoard() {
        ArrayList<ArrayList<Map<String, Long>>> rawBoard = (ArrayList<ArrayList<Map<String, Long>>> ) driver.executeScript("return GameManager._instance.grid.cells;");
        long board = 0x0;
        int pos = 0;
        
        for (int i=0; i < 4; i++) {
            for (int j=0; j < 4; j++) {
                if (rawBoard.get(3-j).get(3-i) != null) {
                    long value =  rawBoard.get(3-j).get(3-i).get("value");
                    value = (long) (Math.log(value) / Math.log(2));
                    board |= value << pos;
                }
                pos += 4;
            }
        }
        return board;
    }

    public static void main(String[] args) throws InterruptedException{

        GameController b = new GameController();
        b.setup();
        Thread.sleep(200);
        long board = b.getBoard();
        int nextMove = Expectimax.run(new State(board,-1), 8, true, 1).move;
        
        while (nextMove != -1) {
        	Thread.sleep(10);
            b.move(nextMove);
            Thread.sleep(10);
            board = b.getBoard();
            nextMove = Expectimax.run(new State(board,-1), 8, true, 1).move;
        }

        System.out.println("Game over");
        Thread.sleep(1000);
        b.stop();
    }
}