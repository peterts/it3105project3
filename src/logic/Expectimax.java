package logic;

import java.util.ArrayList;

public class Expectimax {
	private final static boolean[] IS_TERMINAL = new boolean[65536];
	
	static {
		for(int row = 0; row < 65536; row++){
			IS_TERMINAL[row] = true;
			int prev = 0;
			for(int i=0; i < 4; i++){
				int tile = (row >> 4*i) & 0xf;
				if(tile==0){
					IS_TERMINAL[row] = false;
					break;
				}else{
					if(tile==prev){
						IS_TERMINAL[row] = false;
						break;
					}
					prev = tile;
				}
			}
		}
	}
	
	private static long transpose(long board){
		long a1 = board & 0xf0f00f0ff0f00f0fL;
		long a2 = board & 0x0000f0f00000f0f0L;
		long a3 = board & 0x0f0f00000f0f0000L;
		long a = a1 | (a2 << 12) | (a3 >> 12);
		long b1 = a & 0xff00ff0000ff00ffL;
		long b2 = a & 0x00ff00ff00000000L;
		long b3 = a & 0x00000000ff00ff00L;
		return b1 | (b2 >> 24) | (b3 << 24);
	}
	
	private static boolean isTerminal(long board){
		for(int i = 0; i < 4; i++){
			int row = (int) ((board >> 16*i) & 0xffff);
			if(!IS_TERMINAL[row]){
				return false;
			}
		}
		return true;
	}
	
	private static boolean isTerminal(State node){
		if(isTerminal(node.board)){
			if(isTerminal(transpose(node.board))){
				return true;
			}
		}
		return false;
	}
	
	private static ArrayList<State> maxChildren(State parent){
		ArrayList<State> children = new ArrayList<State>();
		long board = parent.board;
		State child = Move.swipeLeft(board);
		if (child.board != board) {
			children.add(child);
		}
		child = Move.swipeRight(board);
		if (child.board != board) {
			children.add(child);
		}
		child = Move.swipeUp(board);
		if (child.board != board) {
			children.add(child);
		}
		child = Move.swipeDown(board);
		if (child.board != board) {
			children.add(child);
		}
		return children;
	}
	
	private static ArrayList<ChanceState> chanceChildren(State parent){
		return Move.spawnTiles(parent.board);
	}

	private final static float LARGE_NEGATIVE_VALUE = -1000000.0f;
	private final static float TRESHOLD = 0.001f;
	
	public static State run(State node, int depth, boolean maxPlayer, float cumProb){
		if(isTerminal(node)){
			return new State(LARGE_NEGATIVE_VALUE);
		}
		
		if(depth==0 || cumProb < TRESHOLD){
			node.value = Heuristics.hScore(node.board);
			return node;
		}
		
		if(maxPlayer){
			State best = new State(Float.MIN_VALUE);
			for(State child : maxChildren(node)){
				child.value = run(child,depth-1, false, cumProb).value;
				if(best.value < child.value){
					best = child;
				}
			}
			return best;
		}else{
			float value = 0.0f;
			for(ChanceState child : chanceChildren(node)){
				child.value = run(child,depth-1, true, cumProb*child.probability).value;
				value += child.value * child.probability;
			}
			return new State(value);
		}
	}
}
