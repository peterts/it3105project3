package logic;

public class Heuristics {
	private static final float CONSTANT = 100000f;
	private static final float W_FREETILES = 1000f;
	private static final float W_MONOTONICITY = 10;
	private static final float W_TIDYNESS = 100f;
	private static final float POW_MONOTONICITY = 4.0f;

	private static final float[] HEURISTICS = new float[65536];

	static {
		for (int row = 0; row < 65536; row++) {
			HEURISTICS[row] = hScore(row);
		}
	}

	private static float hScore(int row) {
		// free tiles and size
		int free = 0;
		int tidyness = 0;
		int prev = 0;
		for (int i = 0; i < 4; i++) {
			int tile = (row >> i * 4) & 0xf;
			if (tile == 0) {
				free += 1;
			}else if(tile > 0 ){
				if(tile!=prev){
					tidyness -= 1;					
				}
				prev = tile;
			}
		}

		// monotonicity
		int monoLeft = 0;
		int monoRight = 0;
		for (int i = 1; i < 4; i++) {
			int left = (row >> 4 * i) & 0xf;
			int right = (row >> 4 * (i - 1)) & 0xf;
			if (left > right) {
				monoLeft += Math.pow(left, POW_MONOTONICITY)
						- Math.pow(right, POW_MONOTONICITY);
			} else {
				monoRight += Math.pow(right, POW_MONOTONICITY)
						- Math.pow(left, POW_MONOTONICITY);
			}
		}

		return CONSTANT + W_FREETILES * free + W_TIDYNESS * tidyness
				- W_MONOTONICITY * Math.min(monoRight, monoLeft);
	}

	private static long transpose(long board) {
		long a1 = board & 0xf0f00f0ff0f00f0fL;
		long a2 = board & 0x0000f0f00000f0f0L;
		long a3 = board & 0x0f0f00000f0f0000L;
		long a = a1 | (a2 << 12) | (a3 >> 12);
		long b1 = a & 0xff00ff0000ff00ffL;
		long b2 = a & 0x00ff00ff00000000L;
		long b3 = a & 0x00000000ff00ff00L;
		return b1 | (b2 >> 24) | (b3 << 24);
	}

	public static float hScore(long board) {
		int m = 0xffff;
		long boardT = transpose(board);
		float score = 0;
		for (int i = 0; i < 4; i++) {
			int row = (int) ((board >> i * 16) & m);
			int col = (int) ((boardT >> i * 16) & m);
			score += HEURISTICS[row];
			score += HEURISTICS[col];
		}
		return score;
	}
}
