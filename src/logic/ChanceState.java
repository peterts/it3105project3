package logic;

public class ChanceState extends State{
	public float probability; //The probability of reaching this state

	public ChanceState(long board, float probability){
		super(board);
		this.probability = probability;
	}
}
