package logic;

import java.util.ArrayList;

public class Move {
	public final static char[] MERGE_LEFT = new char[65536];
	public final static char[] MERGE_RIGHT = new char[65536];

	static {
		for (int row = 0; row < 65536; row++) {
			MERGE_LEFT[row] = mergeLeft((char) row);
			MERGE_RIGHT[row] = mergeRight((char) row);
		}

	}
	
	private static char mergeLeft(char row) {
		long m1 = 0xf000;
		long m2 = 0x00ff;
		int j = 0;
		for (int i = 0; i < 3; i++) {
			int tile1 = (row >> 4*(3-j)) & 0xf;
			int tile2 = (row >> 4*(2-j)) & 0xf;
			if (tile1 == 0 || tile2 == 0) {
				row = (char) ((row & m1) | ((row & ~m1) << 4));
			} else {
				if (tile1 == tile2) {
					row = (char) ((row & m1) + (0x1 << 4*(3-j)) | ((row & m2) << 4));
				}
				m1 |= m1 >> 4;
				m2 >>= 4;
				j++;
			}
		}
		return row;
	}
	
	private static char mergeRight(char row) {
		long m1 = 0x000f;
		long m2 = 0xff00;
		int j = 0;
		for (int i = 0; i < 3; i++) {
			int tile1 = (row >> 4*j) & 0xf;
			int tile2 = (row >> 4*(j+1)) & 0xf;
			if (tile1 == 0 || tile2 == 0) {
				row = (char) ((row & m1) | ((row & ~m1) >> 4));
			} else {
				if (tile1 == tile2) {
					row = (char) ((row & m1) + (0x1 << 4*j) | ((row & m2) >> 4));
				}
				m1 |= m1 << 4;
				m2 <<= 4;
				j++;
			}
		}
		return row;
	}
	
	private static long transpose(long board){
		long a1 = board & 0xf0f00f0ff0f00f0fL;
		long a2 = board & 0x0000f0f00000f0f0L;
		long a3 = board & 0x0f0f00000f0f0000L;
		long a = a1 | (a2 << 12) | (a3 >> 12);
		long b1 = a & 0xff00ff0000ff00ffL;
		long b2 = a & 0x00ff00ff00000000L;
		long b3 = a & 0x00000000ff00ff00L;
		return b1 | (b2 >> 24) | (b3 << 24);
	}
	
	private static long _swipeRight(long board) {
		long m = 0xffff;
		for (int i = 0; i < 4; i++) {
			board = (board & ~m) | (long) MERGE_RIGHT[(char) ((board & m) >> i * 16)] << i * 16;
			m <<= 16;
		}
		return board;
	}
	
	private static long _swipeLeft(long board) {
		long m = 0xffff;
		for (int i = 0; i < 4; i++) {
			board = (board & ~m) | (long) MERGE_LEFT[(char) ((board & m) >> i * 16)] << i * 16;
			m <<= 16;
		}
		return board;
	}
	
	public static State swipeUp(long board){
		return new State(transpose(_swipeLeft(transpose(board))),0);
	}
	
	public static State swipeRight(long board){
		return new State(_swipeRight(board),1);
	}
	
	public static State swipeDown(long board){
		return new State(transpose(_swipeRight(transpose(board))),2);
	}
	
	public static State swipeLeft(long board){
		return new State(_swipeLeft(board),3);
	}
	
	public static ArrayList<ChanceState> spawnTiles(long board){
		ArrayList<ChanceState> children = new ArrayList<ChanceState>();
		int n = 0;
		long m1 = 0xf;
		long m2 = 0x1;
		long m3 = 0x2;
		for(int i = 0; i < 16; i++){
			if((board & m1) == 0){
				children.add(new ChanceState(board & ~m1 | m2, 0.9f));
				children.add(new ChanceState(board & ~m1 | m3, 0.1f));
				n++;
			}
			m1 <<= 4;
			m2 <<= 4;
			m3 <<= 4;
		}
		for(State c : children){
			((ChanceState)c).probability *= (float)1/n;
		}
		return children;
	}
}