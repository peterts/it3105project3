package logic;

public class State{
	public long board; //the board state
	public float value; //the value of this board state
	public int move; //the move that caused this board state
	public int depth; //the depth at which this state occurred
	
	public State(long board){
		this.board = board;
	}
	
	public State(long board, int move){
		this.board = board;
		this.move = move;
	}
	
	public State(float value){
		this.value = value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (board ^ (board >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		State other = (State) obj;
		if (board != other.board)
			return false;
		return true;
	}
}
